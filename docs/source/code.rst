Documentation for SillyWalks
****************************

Walk-Simulations
================

.. automodule:: sillywalks.walker.interpolatedWalk
   :members:
      

.. autoclass:: sillywalks.walker.simulatedWalk.SimulatedWalker
   :members:
   :undoc-members:
   :show-inheritance:


Utilities
=========

GridMap-Creation
----------------
.. automodule:: sillywalks.mapCreation
   :members:
   :undoc-members:
   :show-inheritance:
   
Noise-Generation
----------------
.. automodule:: sillywalks.noiseGeneration
   :members:
   :undoc-members:
   :show-inheritance:

Validation
----------
.. automodule:: sillywalks.validation
   :members:
   :undoc-members:
   :show-inheritance:
