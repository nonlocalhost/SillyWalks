.. SillyWalks documentation master file, created by
   sphinx-quickstart on Mon Oct 30 21:16:06 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SillyWalks's documentation!
======================================

Overview of all modules with documentation:

.. toctree::
   :maxdepth: 2
      
   code.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
